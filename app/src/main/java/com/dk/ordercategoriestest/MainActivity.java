package com.dk.ordercategoriestest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dk.ordercategoriestest.model.Category;
import com.dk.ordercategoriestest.model.Filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Filter filter1 = new Filter("Filter 1", 3);
        Filter filter2 = new Filter("Filter 2", 6);
        Filter filter3 = new Filter("Filter 3", 5);
        Filter filter4 = new Filter("Filter 4", 2);
        Filter filter5 = new Filter("Filter 5", 4);
        Filter filter6 = new Filter("Filter 6", 1);

        List<Category> categories = new ArrayList<>();
        categories.add(new Category("Category 1", new Filter[]{filter1, filter2, filter3}));
        categories.add(new Category("Category 2", new Filter[]{filter4, filter5, filter6}));
        categories.add(new Category("Category 3", new Filter[]{filter1, filter3, filter5}));
        categories.add(new Category("Category 4", new Filter[]{filter2, filter4, filter6}));
        Collections.sort(categories, Collections.<Category>reverseOrder());

        ListView lvCategories = (ListView) findViewById(R.id.lvCategories);
        lvCategories.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categories));
    }
}
