package com.dk.ordercategoriestest.model;

import android.support.annotation.NonNull;

public class Category implements Comparable<Category> {

    private String name;
    private Filter[] filters;

    public Category(String name, Filter[] filters) {
        this.name = name;
        this.filters = filters;
    }

    public String getName() {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public Filter[] getFilters() {
        return filters;
    }

    public void setFilters(Filter[] filters) {
        this.filters = filters;
    }

    private int getRatingSum() {
        int sum = 0;
        for (Filter filter : filters)
            sum += filter.getRating();
        return sum;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(@NonNull Category other) {
        return Integer.compare(getRatingSum(), other.getRatingSum());
    }
}
